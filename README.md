# README #

This project shows how to use EF6 with ASP.NET 5 and MVC6. Note that EF7 (or EF .NET Core 1.0) is very feature limited right now.

### How do I get set up? ###

#### Setup ####
* Clone the project
* Setup a database called SimpleTest (by default on a named instanced called SQL2012)
* Build the project
* Set the startup project to be DataLayer
* Run Enable-Migrations
* Run Update-Database
* Switch the startup project back to SimpleTestApplication
* Launch in IISExpress or Kestrel
* Navigate to the About page and observe all of the EF6 goodies displayed on the page