﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    class TestDataContext : DbContext 
    {
        public TestDataContext() : base(@"Persist Security Info=False;Integrated Security = true; Initial Catalog=SimpleTest;Server=.\SQL2012") { }
        public DbSet<TestData> TestDatas { get; set; }
    }
}
