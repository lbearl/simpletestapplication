﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class TestDataRepository
    {
        public List<TestData> GetAllTestData()
        {
            using (var context = new TestDataContext())
            {
                return context.TestDatas.ToList();
            }
        }

    }
}
