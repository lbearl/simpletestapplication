﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class TestData
    {
        public int Id { get; set; }
        public string TestText { get; set; }
    }
}
